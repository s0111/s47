import './App.css';
import React from 'react'
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import LogIn from './pages/LogIn';
import Logout from './pages/Logout';
import Error from './pages/Error';
import { Container } from 'react-bootstrap';

//For Routes
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'


function App() {
  return (
    <Router>
      <AppNavbar />
      <Container>
    
        <Routes >
          < Route path="/" element = { <Home /> } />
          < Route path="/courses" element = { <Courses /> } />
          < Route path="/register" element = { <Register /> } />
          < Route path="/login" element = { <LogIn /> } />
          < Route path="/logout" element = { <Logout /> } />
          <Route path='*'  element ={ <Error /> } />
        </Routes> 
 
      </Container >
    </Router>
  );
}

export default App;
