const courseData = [
    {
        id: "wdc001",
        name: "How To be A Kangaroo",
        description: "Basic course on how to be a kangaroo in Australia",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Python - Django",
        description: "Pariatur et enim nostrud aliquip amet voluptate consectetur nisi adipisicing sit adipisicing.",
        price: 50000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Java - Springboot",
        description: "Pariatur et enim nostrud aliquip amet voluptate consectetur nisi adipisicing sit adipisicing.",
        price: 55000,
        onOffer: true
    },
]
export default courseData;