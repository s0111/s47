// import React from 'react';
import React, { useState, useEffect } from 'react';
import {Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function CourseCard ({courseProp}) {
    const { name, description, price } = courseProp;

    const [count, setCount] = useState(0);
    const [slot, setSlot] = useState(30);
    //statehook that indicates availability of course for enrollment(enroll)
	const [isOpen, setIsOpen] = useState(true)

	console.log(count);


	const enroll = () => {
		setCount(count + 1);
        console.log("Enrollees: " + count);
        setSlot(slot - 1);
        console.log("Slot: " + slot);
		

        // if (slot === 0 && count === 30) {
        //     alert("Course already fully booked, there is no more slot!");
        // }
	}

    //When you call useEffect, you're telling React to run you "effect" function after flushing changes to the DOM. Effects are declared inside the component so they have access to its props and states
	useEffect(() => {
		if(slot === 0) {
			setIsOpen(false);
		}
	}, [slot])//it controls the rendering of the useEffect


    return (
      
                <Card className="m-3">
                    <Card.Body>
                        <Card.Title> { name } </Card.Title>
                        <Card.Subtitle>Description: </Card.Subtitle>
                        <Card.Text> { description } </Card.Text>
                        <Card.Subtitle>Price: </Card.Subtitle>
                        <Card.Text>Php { price }</Card.Text>
                        <Card.Subtitle>Enrollees: {count}</Card.Subtitle>
                        <Card.Text>slot:{slot}</Card.Text>
                        
                        {isOpen?
                        <Button variant = "primary" onClick={enroll}>Enroll now!</Button>
                        :
                        <Button variant="primary" disabled>Enroll</Button>
                        }
                    </Card.Body>
                </Card>
    )
}

CourseCard.propTypes = {
	//shape() method it is used to check if a prop object conforms to a specific 'shape'
	courseProp: PropTypes.shape({
		//Define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
