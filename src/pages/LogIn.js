import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';



export default function LogIn() {

	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	

	//State to determine whether submit button is enabled or not for conditional rendering
	const [isActive, setIsActive] = useState(true);


	useEffect(() => {
		//Validation to enable submit button when all fields are populated and both passwords match
		if((email !== '' && password1 !== '')){
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	}, [email, password1])


	function LoginUser(e) {
		//Prevents page redirection via form submission
		e.preventDefault();

		//Set the email of the authenticated user in the localStorage
		//localSorage.setItem('propertyName' , Value)
		localStorage.setItem('email', email);

		//Clear input fields
		setEmail('');
		setPassword1('');
		

		Swal.fire({
			title: "Welcome!",
			icon: "success",
			text: "You have successfully Logged In"
		})

	}

	return (

        <Form onSubmit={(e) => LoginUser(e)} className ="my-4">
		<h1>LogIn</h1>
		<Form.Group> 
			<Form.Label>Email Adress</Form.Label>
			<Form.Control 
				type="email"
				placeholder="Enter Email"
				required
				value={email}
				onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else
				</Form.Text>
		</Form.Group>

		<Form.Group> 
			<Form.Label>Password</Form.Label>
			<Form.Control 
				type="password"
				placeholder="Enter Password"
				required
				value={password1}
				onChange={e => setPassword1(e.target.value)}
				/>
		</Form.Group>

		{/* <Form.Group> 
			<Form.Label>Verify Password</Form.Label>
			<Form.Control 
				type="password"
				placeholder="Verify Password"
				required
				value={password2}
				onChange={e => setPassword2(e.target.value)}
				/>
		</Form.Group> */}
		{isActive ? 
			<Button variant="primary" type="submit" className="mt-3">LogIn</Button>

			:

			<Button variant="danger" type="submit" className="mt-3" disabled>LogIn</Button>

		}
		
	</Form>
    
		)
}
