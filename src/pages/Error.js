import React from 'react';
import { Row , Col} from 'react-bootstrap';
import { Link } from 'react-router-dom'

export default function Error() {


    return (
        <div>
               <h1 className ="my-3"> 404 -PAGE NOT FOUND!</h1>
               {/* <p>Return to <a href = "/">HomePage</a></p> */}
              <p>Go back <Link to = "/">Homepage</Link></p>
        </div>
    )
}